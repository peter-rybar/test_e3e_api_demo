# Find movies

Find and book movie tickets

|Location   |Movie      |Theatre    |Time   |
|-----------|-----------|-----------|-------|
|Bangalore  |Star Wars  |inox       |19.30  |
|Mumbai     |Star Wars  |inox       |14.20  |

## Search for movies

- Specify location as `Location`
- Search for movie `Movie`
- [ ] Verify that `Theatre` is playing `Movie` at `Time`

## Book movie ticket

- Select movie `Movie`
- Click button `Book ticket`
- [ ] Verify that ticket with QR code was displayed for `Movie` at `Time`
