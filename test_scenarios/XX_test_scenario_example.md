# Find movies

We are creating a new website to find and book movie tickets

|Location   |Movie      |Theatre    |Time   |
|-----------|-----------|-----------|-------|
|Bangalore  |Star Wars  |inox       |19.30  |
|Mumbai     |Star Wars  |inox       |14.20  |

## Search for movies

- Specify location as `Location`
- Search for movie `Movie`
- [ ] Verify that `Theatre` is playing `Movie` at `Time`
