# End to End tests

Test scenario template/example: [XX_test_scenario_example.md](XX_test_scenario_example.md)

Meaning:

> - [ ] Test isn't automated yet
> - [x] Test is automated

## E2E test scenarios

- [x] [Find movie](01_find_movie.md)
- [ ] [Test scenario example](XX_test_scenario_example.md)
