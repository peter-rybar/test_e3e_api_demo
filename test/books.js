const expect = require('chai').expect;
const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');

// Chai assertions https://www.chaijs.com/api/bdd/

describe('gRPC API Books', () => {

    let client;

    before(() => {
        const packageDefinition = protoLoader.loadSync('./test/proto/bookStore.proto', {});
        const bookStorePackage = grpc.loadPackageDefinition(packageDefinition).bookStorePackage;
        client = new bookStorePackage.Book('localhost:50051', grpc.credentials.createInsecure());
    });

    it('Book create', (done) => {
        const book = 'Cracking the Interview';
        client.createBook(
            { id: -1, book: book },
            (err, response) => {
                if (err) {
                    // console.log(err);
                    done(err);
                } else {
                    // console.log(JSON.stringify(response));
                    expect(response)
                        .to.have.property('book')
                        .that.is.a('string')
                        .and.to.equal(book);
                    done();
                }
            }
        );

    });

    it('Book read', (done) => {
        const book = 'Cracking the Interview';
        client.readBook({ id: 1 }, (err, response) => {
            if (err) {
                // console.log(err);
                done(err);
            } else {
                // console.log(JSON.stringify(response));
                expect(response)
                    .to.have.property('book')
                    .that.is.a('string')
                    .and.to.equal(book);
                done();
            }
        });
    });

    it('Books read', (done) => {
        const book = 'Cracking the Interview';
        client.readBooks(null, (err, response) => {
            if (err) {
                // console.log(err);
                done(err);
            } else {
                // console.log(JSON.stringify(response));
                expect(response)
                    .to.have.property('books')
                    .to.be.an('array');
                expect(response)
                    .to.deep.nested.include({ 'books[0].book': book });
                done();
            }
        });
    });
})

